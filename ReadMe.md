# ENSAE Distributed Computing

This repository gathers all the materials for the Distributed Computing course of ENSAE Paristech

## Usage

First, build the environment for the service

```bash
docker-compose build ensae
```

Secondly, launch the container
```bash
docker-compose run -d ensae
```

By default, your jupyter server will be available at http://localhost:8888.

The token is in the logs of the container. Please check, them with the command docker logs.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
