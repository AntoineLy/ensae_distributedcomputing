from multiprocessing import Process
import useYourOwnFunctions



if __name__ == '__main__':
    jobs = []
    for i in range(5):
        p = Process(target=useYourOwnFunctions.f, args=(i,))
        jobs.append(p)
        p.start()
        p.join()
    print('p',jobs)
