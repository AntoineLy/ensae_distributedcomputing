from multiprocessing import Process
import os

def info(title):
    print(title)
    print('module name:', __name__)
    print('parent process:', os.getppid())
    print('process id:', os.getpid())

def f(name):
    info('function f')
    print('Arguments', name)

if __name__ == '__main__':
    jobs = ['Test1',"Test2","Test3"]
    info('main line')
    for i in jobs:
        p = Process(target=f, args=(i,))
        p.start()
        p.join()
