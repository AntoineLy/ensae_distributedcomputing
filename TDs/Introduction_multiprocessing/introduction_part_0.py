from multiprocessing import Process

def worker():
    """worker function"""
    print('Hello')
    return

if __name__ == '__main__':
    jobs = []
    for i in range(5):
        p = Process(target=worker)
        jobs.append(p)
        p.start()
