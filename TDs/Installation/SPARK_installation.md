## Installation de SPARK

Nous détaillons ici les étapes effectuées afin d'installer SPARK en mode standalone au sein de la machine virtuelle.

### Première étape: s'assurer d'une version à jour de Java

Spark est un framework produit par Apache qui s'appuie sur le langage Java. Il donc prérequis d'avoir une version à jour de Java. Les versions stables de Java compatibles avec Spark sont les versions ultérieures à la version 7. Cette étape peut être ignorée si java est déjà convenablement installé. Pour le vérifier:

1. Ouvrir un terminal (raccourci: CTRL+ALT+T)
2. Excecuter la commande >java -version

si java est correctement installé, vous devriez voir apparaitre quelque chose comme: 
		
	openjdk version "1.8.0_151"
	OpenJDK Runtime Environment (build 1.8.0_151-8u151-b13-0ubuntu1.16.04.2-b13)
	OpenJDK 64-Bit Server VM (build 25.151-b13, mixed mode)

Si ce n'est pas le cas, les étapes suivantes permettent d'installer Java.

	sudo apt-get update
	sudo apt-get install default-jdk

Assurez vous ensuite que votre variable d'environnement `JAVA_HOME` soit bien définie. Pour cela:

1. Ouvrez votre .bashrc avec l'éditeur de votre choix (nano, vi, vim, etc.)

	nano ~/.bashrc

2. Ajouter à la fin du fichier la variable

	export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-amd64

3. Sauvegarder et fermer

De manière alternative vous pouvez également installer manuellement java (si l'installation a réussi vous pouvez passer à [Télécharger Spark](#télécharger-spark)).

Téléchargement de la version 8 du jdk disponible sur le site officiel de java (http://www.oracle.com/technetwork/java/javase/downloads/java-archive-javase8-2177648.html)

Une version installée peut par exemple être `jdk-8u151-linux-x64.tar.gz` où une version ultérieure.

Une fois téléchargée, vous devez alors décompresser le fichier et le placer dans le répertoire de votre choix. Le fichier a par exemple été décompressé dans le dossier `spark-hadoop` dans ce qui suit.

Les commandes excécutées depuis le terminal sont plus précisément les suivantes:

> mkdir spark-hadoop 
> cd spark-hadoop
> tar xzf [CHEMIN_OU_VOUS_AVEZ_TELECHARGE_LE_JDK]/jdk-8u151-linux-x64.tar.gz

Une fois cette operation effectuée vous devriez voir apparaitre le dossier `jdk-8u151-linux-x64` dans le répertoire `spark-hadoop`. 

Une fois décompressée, vous pouvez excécuter les commandes suivantes afin de préciser la localisation de la mise à jour de Java. Cette étape est facultative pour l'installation de SPARK. Une simple variable d'environnemdent `JAVA_HOME`peut être définie à la place.

>sudo  update-alternatives --install /usr/bin/jar jar /opt/jdk1.8.0_51/bin/jar 1
sudo  update-alternatives --install /usr/bin/java java /opt/jdk1.8.0_51/bin/java 1
sudo   update-alternatives --install /usr/bin/javac javac /opt/jdk1.8.0_51/bin/javac 1
sudo   update-alternatives --install /usr/bin/jps jps /opt/jdk1.8.0_51/bin/jps 1
sudo   update-alternatives --set jar /DeZyre/jdk1.8.0_51/bin/jar
sudo   update-alternatives --set javac /DeZyre/jdk1.8.0_51/bin/javac

### Télécharger SPARK

Vous pouvez télécharger le framework sous sa forme pré compilée sur le site officiel de SPARK (https://spark.apache.org/downloads.html) qui propose différentes versions. Par défaut la dernière version stable recommandée est disponible. 

Télécharger la version "pre-built" et la décompresser  dans par exemple le dossier `spark-hadoop` (ce dossier est à créer si vous ne l'avez pas ou le remplacer par le module de votre choix) en suivant les instructions suivantes:

1. Ouvrir un terminal (raccourci: CTRL+ALT+T)
2. Executer les commandes suivantes:
> cd spark-hadoop 
> wget https://downloads.apache.org/spark/spark-2.4.5/spark-2.4.5-bin-hadoop2.7.tgz 
> tar -xvf [CHEMIN_OU_VOUS_AVEZ_TELECHARGE_SPARK]/spark-2.4.5-bin-hadoop2.7.tgz 


Une fois cette operation effectuée vous devriez voir apparaitre le dossier `spark-2.4.5-bin-hadoop2.7` dans le répertoire `spark-hadoop` (le nom peut changer en fonction de la distribution). Vous pouvez dès à présent lancer spark en standalone en vous plaçant dans le répertoire de spark et en appelant les excécutables:

> cd ~/spark-hadoop/spark-2.4.5-bin-hadoop2.7/bin
> sudo spark-shell


Afin de pouvoir lancer spark plus facilement depuis le terminal sans avoir à chaque fois à nous placer dans le répertoire approprié, nous allons alors éditer la variable d'environnement `PATH`. 

**A propos de la variable d'environnement `PATH`**
Avant toute chose, une variable d'environnement est une variable globale définie et utilisable par le système. Ce type de variable est notamment très utile pour stocker des informations dans votre système sans avoir à les écrire dans un document et de pouvoir y accèder rapidement. Sur Linux, depuis le terminal vous pouvez visualiser les variables définies en tapant la commande `env` (sous windows, depuis votre terminal la commande `SET` est équivalente). 

Parmi ces variables d'environnement, une est particulièrement exploitée par les OS: la variable `PATH`. Pour faire simple, lorsque dans un terminal une commande est appelée (par exemple lorsque vous tapez `jupyter notebook`), l'OS parcours en générale cette variable d'environnement où se trouvent de nombreux chemins de dossiers et va rechercher un exécutable portant le nom de la commande que vous lui demandez (en l'occurence ici `jupyter notebook`). Afin de visualiser la valeur de la variable d'environnement `PATH` vous pouvez soit la chercher dans la liste des variables retournées par la commande `env` ou bien excécuter la commande suivante :

> echo $PATH

Ainsi si l'on veut que la commande `spark-shell` puisse être appelée, il va falloir ajouter dans la liste des chemins enregistrés le chemin du dossier où se trouver l'excécutable. Pour se faire nous allons excécuter depuis le terminal la commande suivante: 

> SPARK_HOME="/home/[USER]/spark-hadoop/spark-2.4.5-bin-hadoop2.7"
> export PATH=$SPARK_HOME/bin:$SPARK_HOME/sbin:$PATH

Une fois excécutée, vous devriez pouvoir simplement appeler la commande `spark-shell`. 

 Si vous essayez désormais d'ouvrir un nouveau terminal et d'appeler `spark-shell` vous remarquerez que cela ne foncitonne plus. Ceci est tout à fait normal. Nous avons ici occulté une propriétée des variables d'environnement. Les variables créées dans un terminal n'existent en effet qu'au sein de ce terminal. Alors comment faire pour s'assurer que la variable que nous avons édité tout à l'heure le soit tout le temps? Nous allons utilisé ici un fichier qui est tout le temps appelé lorsque vous ouvrez un terminal sous Linux: le `.bashrc`. Ce fichier est appelé à chaque fois au lancement d'un terminal. Nous allons donc l'éditer afin d'ajouter définitevement les lignes de commandes précedemment excécutées. 

1. Ouvrir un terminal
2. Ouvrir le fichier `.bashrc` en excécutant la commande :
> vim .bashrc 
3. Une fenêtre d'édition s'ouvre. Vous pouvez alors vous déplacer dans cette fenêtre grâce aux touches directionnelles (dans un premier temps) et olacez vous tout à la fin.
4. Appuyer sur la touche `i` afin d'activer le mode édition
5. Ajouter les lignes 
>SPARK_HOME="/home/[USER]/spark-hadoop/spark"
export PATH=$SPARK_HOME/bin:$SPARK_HOME/sbin:$PATH
6. Désactiver le mode édition en appuyant sur la touche `ECHAP`
7. Quitter et sauvegarder en tapant `:wq` (en cas d'erreur, `:q!`permet de quitter sans sauvegarder)
 
**Note sur VIM**
Vim est un outil d'édition minimal qui peut être utilisé via un terminal. Si cet outil parait rudimentaire il est très pratique notamment lorsque vous voulez éditer un document à distance et que vous ne posséder rien de plus qu'un terminal pour y accèder. Vous pouvez vous contenter du minimum de connaissance sur `vim`pour le moment. Pour en savoir plus, ce tutoriel est par exemple très complet https://openclassrooms.com/courses/reprenez-le-controle-a-l-aide-de-linux/vim-l-editeur-de-texte-du-programmeur 

Une fois cette opération effectuée, vous pouvez ouvrir un nouveau terminal et vérifier que cela fonctionne en tapant la commande `spark-shell`.

__________________________
#### Credits
**Auteur:** Antoine LY
**Contact:** aly@scor.com