## Ajout de kernels Jupyter

Dans cette section, nous détaillons l'ajout d'un noyau Pyspark  dans le notebook Jupyter. Il est donc supposé que Spark, Jupyter et Python ont préalablement été installés. Ces étapes ont déjà été effectuées dans la template.

### Ajouter le kernel pyspark

1. Créer un répertoire pour le nouveau kernel (ce repertoir est un répertoire lu par défaut par jupyter)
> sudo mkdir -p /usr/local/share/jupyter/kernels/pyspark
ou 
> sudo mkdir -p /home/[USER]/miniconda3/envs/pyspark/share/jupyter/kernels/pyspark

2. Se placer dans ce dossier et créer un fichier json
> cd /usr/local/share/jupyter/kernels/pyspark
ou 
> cd /home/[USER]/miniconda3/envs/pyspark/share/jupyter/kernels/pyspark

> sudo vim kernel.json
ou
> nano kernel.json

3. Passer en mode edition en appuyant sur `i`. 
4. Puis copier coller le text ci-dessous

		{
		"display_name": "PySpark",
		  "language": "python",
		  "argv": [ "/home/[USER]/miniconda3/bin/python", "-m", "ipykernel", "-f", "{connection_file}" ],
		  "env": {
		    "PYSPARK_PYTHON": "/home/[USER]/miniconda3/[env]/bin/python",
			"PYTHONSTARTUP": "/home/aly/spark-hadoop/spark-2.4.5-bin-hadoop2.7/python/pyspark/shell_custom.py"

		  }
		}

Exemple 
		 {
                "display_name": "PySpark",
                  "language": "python",
                  "argv": [ "/home/aly/miniconda3/envs/pyspark/bin/python", "-m", "ipykernel", "-f", "{connection_file}" ],
                  "env": {
                    "PYSPARK_PYTHON": "/home/aly/miniconda3/envs/pyspark/bin/python",
                    "PYTHONSTARTUP": "/home/aly/spark-hadoop/spark-2.4.5-bin-hadoop2.7/python/pyspark/shell_custom.py"

                  }
                }


5. Enregistrer et quitter: `ECHAP` puis `:wq`

Code de test à exécuter dans un notebook

	from pyspark import SparkConf
	from pyspark.context import SparkContext
	from pyspark.sql import SparkSession, SQLContext
	spark = SparkSession._create_shell_session()
	sc = spark.sparkContext
	sql = spark.sql
	atexit.register(lambda: sc.stop())


### Ajouter les dépendences

Pour se lancer, le kernel pypsark a besoin d'une dépendance (`py4j`) se situant dans un des dossier de spark. Nous allons alors éditer le fichier `.bashrc` afin de s'assurer qu'au lancement du notebook, la localisation de ce fichier soit bien précisé. 

1. Ouvrir depuis un terminal le `.bashrc` en excécutant la commande suivant :
> vim .bashrc
2. A la fin de ce document, passer en mode édition (appuyer sur `i` puis ajouter les lignes suivantes
		
		 #add config for jupyter notebook
		 # PYTHONPATH is used by python to load the used library. Change it only if you know what you are doing.
		export PYTHONPATH=$PYTHONPATH:$SPARK_HOME/python/:$SPARK_HOME/python/lib/`ls $SPARK_HOME/python/lib/ | grep py4j`
3. Enregistrer et quitter: `ECHAP` puis `:wq` 


Depuis un nouveau terminal, vous pouvez desormais utiliser pyspark.


__________________________
#### Credits
**Auteur:** Antoine LY
**Contact:** aly@scor.com