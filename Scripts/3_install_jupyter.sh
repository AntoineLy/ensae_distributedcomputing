#!/bin/bash
# arguments can be set with create cluster

# # example of arguments
# #{1:-"ensae-spark"}
# #{2:-"s3://ensae-spark/notebooks/"}
	
JUPYTER_PASSWORD=ensae-spark
# setup python 3.5 in the master and workers
export PATH="$HOME/conda/bin:$PATH"
NOTEBOOK_DIR=s3://ensae-spark/notebooks/
BUCKET=$(python -c "print('$NOTEBOOK_DIR'.split('//')[1].split('/')[0])")
FOLDER=$(python -c "print('/'.join('$NOTEBOOK_DIR'.split('//')[1].split('/')[1:-1]))")


# check for master node
#IS_MASTER=false
#if grep isMaster /mnt/var/lib/info/instance.json | grep true;
#then

# Install Jupyter Note book on master and libraries
conda install jupyter
conda install matplotlib plotly bokeh
conda install --channel scikit-learn-contrib scikit-learn==0.18
# jupyter configs
mkdir -p ~/.jupyter
touch ls ~/.jupyter/jupyter_notebook_config.py
HASHED_PASSWORD=$(python -c "from notebook.auth import passwd; print(passwd('$JUPYTER_PASSWORD'))")
echo "c.NotebookApp.password = u'$HASHED_PASSWORD'" >> ~/.jupyter/jupyter_notebook_config.py
echo "c.NotebookApp.open_browser = False" >> ~/.jupyter/jupyter_notebook_config.py
echo "c.NotebookApp.ip = '*'" >> ~/.jupyter/jupyter_notebook_config.py
echo "c.NotebookApp.notebook_dir = '$HOME/$BUCKET/$FOLDER'" >> ~/.jupyter/jupyter_notebook_config.py
echo "c.ContentsManager.checkpoints_kwargs = {'root_dir': '.checkpoints'}" >> ~/.jupyter/jupyter_notebook_config.py

echo 'export PYSPARK_DRIVER_PYTHON=/home/hadoop/conda/bin/jupyter' >> $HOME/.bashrc 
echo 'export PYSPARK_DRIVER_PYTHON_OPTS="notebook --log-level=INFO"' >> $HOME/.bashrc 
echo 'export JAVA_HOME=/etc/alternatives/jre' >> $HOME/.bashrc 

export PYSPARK_PYTHON="$HOME/conda/bin/python3.5"
export PYSPARK_DRIVER_PYTHON="$HOME/conda/bin/jupyter"
export PYSPARK_DRIVER_PYTHON_OPTS="notebook --log-level=INFO"
export PYSPARK_PYTHON=/home/hadoop/conda/bin/python3.5
export JAVA_HOME="/etc/alternatives/jre"


cd ~
sudo cat << EOF > /home/hadoop/jupyter.conf
description "Jupyter"
author      "babbel-data-eng"
 
start on runlevel [2345]
stop on runlevel [016]
 
respawn
respawn limit 0 10
 
chdir /mnt/$BUCKET/$FOLDER
	
script
  		sudo su - hadoop > /var/log/jupyter.log 2>&1 << BASH_SCRIPT
    export PYSPARK_DRIVER_PYTHON="/home/hadoop/conda/bin/jupyter"
    export PYSPARK_DRIVER_PYTHON_OPTS="notebook --log-level=INFO"
    export PYSPARK_PYTHON=/home/hadoop/conda/bin/python3.5
    export JAVA_HOME="/etc/alternatives/jre"
    pyspark
  	   BASH_SCRIPT
 
end script
EOF
sudo mv /home/hadoop/jupyter.conf /etc/init/
sudo chown root:root /etc/init/jupyter.conf
 
# be sure that jupyter daemon is registered in initctl
sudo initctl reload-configuration


if [[ -z $`grep isMaster /mnt/var/lib/info/instance.json | grep true` ]]; then
	echo "isNode" 
else
	echo "isMaster"
	#sudo initctl start jupyter
fi

