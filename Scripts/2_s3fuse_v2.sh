#!/bin/bash

######

# mount s3fuse 

######

# extract BUCKET and FOLDER to mount from NOTEBOOK_DIR
NOTEBOOK_DIR=$1
BUCKET=$(python -c "print('$NOTEBOOK_DIR'.split('//')[1].split('/')[0])")
FOLDER=$(python -c "print('/'.join('$NOTEBOOK_DIR'.split('//')[1].split('/')[1:-1]))")

# install s3fs
# install dependencies

sudo yum install -y git
sudo yum install -y libcurl libcurl-devel graphviz
sudo yum install -y cyrus-sasl cyrus-sasl-devel readline readline-devel gnuplot
sudo yum install -y automake fuse fuse-devel gcc-c++ libxml2-devel make openssl-devel
sudo yum groupinstall -y 'Development Tools'

cd /mnt
git clone https://github.com/s3fs-fuse/s3fs-fuse.git
cd s3fs-fuse/
ls -alrt
./autogen.sh
./configure
make
sudo make install
sudo su -c 'echo user_allow_other >> /etc/fuse.conf'
mkdir -p /mnt/s3fs-cache
mkdir -p ~/$BUCKET
/usr/local/bin/s3fs -o allow_other -o iam_role=auto -o umask=0 -o url=https://s3.amazonaws.com  -o no_check_certificate -o enable_noobj_cache -o use_cache=/mnt/s3fs-cache $BUCKET ~/$BUCKET
